import main.scala.BarrowsWiller
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class EncodingSuite extends FunSuite {
  import main.scala.BarrowsWiller.encode
  val input = "the cat in the car ate the rat"

  test("the cat") {
    assert(encode(input.size, input * 2 dropRight(1) iterator) == ("reetenecc r  hhhtttt ia aa t a",28))
  }
}