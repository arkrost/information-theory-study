import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class EncodingStaffSuite extends FunSuite {
  import EncodingStaff._
  val input = "abaccaba"

  test("probability") {
    assert(probabilities(input) == Map('a' -> 0.5, 'b' -> 0.25, 'c' -> 0.25))
  }

  test ("intervals") {
    val distribution = probabilities(input)
    assert(intervals(distribution).map(x => (x._1, x._2._2 - x._2._1)) == distribution)
  }
}
