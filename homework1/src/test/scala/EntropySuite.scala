import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class EntropySuite extends FunSuite {
  import Entropy.{H1, H2}
  val s = "Кукушка кукушонку купила капюшон. Как в капюшоне он смешон!" .toCharArray
  val EPS = 1e-6
  def suitable(expected: Double, got: Double) = math.abs(expected - got) < EPS

  test("2 * H1(2) == 4.8929091253") {
    assert(suitable(4.8929091253 / 2, H1(2)(s iterator)), "Got: " + H1(2)(s iterator))
  }

  test("3 * H1(3) == 5.3767496633") {
    assert(suitable(5.3767496633 / 3, H1(3)(s iterator)), "Got: " + H1(3)(s iterator))
  }

  test("4 * H1(4) == 5.5930692078") {
    assert(suitable(5.5930692078 / 4, H1(4)(s iterator)), "Got: " + H1(4)(s iterator))
  }

  ignore ("wrong test1") {
    test("2 *H2(2) == 15.9996304105") {
     assert(suitable(15.9996304105 / 2, H2(2)(s iterator)), "Got: " + H2(2)(s iterator))
    }
  }

  ignore ("wrong test2") {
    test("3 * H2(3) == 23.9999994822") {
      assert(suitable(23.9999994822 / 3, H2(3)(s iterator)), "Got: " + H2(3)(s iterator))
    }
  }

  ignore ("wrong test3") {
    test("4 * H2(4) == 31.9999995848") {
      assert(suitable(31.9999995848 / 4, H2(4)(s iterator)), "Got: " + H2(4)(s iterator))
    }
  }
}
