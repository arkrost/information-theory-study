import collection.mutable
object AdaptiveA {
  import EncodingStaff._

  def encode(input : String) : Int = {
    val t = mutable.Map.empty[Char, Int]
    var G = BigDecimal(1)
    var n = 0
    input.foreach(x => {
      if (t contains x) {
        G = G * t(x) / (n + 1)
        t(x) = t(x) + 1
      } else {
        t += x -> 1
        G = G / (n + 1) / (256 - t.size)
      }
      n = n + 1
      println(n + " " +G)
    })
    roundedLog(1 / G) + 1
  }

  def main(args : Array[String]) {
    val text = "IF_WE_CANNOT_DO_AS_WE_WOULD_WE_SHOULD_DO_AS_WE_CAN"
    println(encode(text))
  }
}

import collection.mutable
object AdaptiveD {
  import EncodingStaff._

  def encode(input : String) : Int = {
    val t = mutable.Map.empty[Char, Int]
    var G = BigDecimal(1)
    var n = 1
    input.foreach(x => {
      if (t contains x) {
        G = G * (t(x) - 0.5) / n
        t(x) = t(x) + 1
      } else {
        t += x -> 1
        G = G * t.size / (n << 1) / (256 - t.size)
      }
      n = n + 1
      println(n + " " +G)
    })
    roundedLog(1 / G) + 1
  }

  def main(args : Array[String]) {
    val text = "IF_WE_CANNOT_DO_AS_WE_WOULD_WE_SHOULD_DO_AS_WE_CAN"
    println(encode(text))
  }

}

