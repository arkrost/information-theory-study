import collection.mutable

object EncodingStaff {
  def probabilities(input : String): Map[Char, Double] = input
    .foldLeft(mutable.Map.empty[Char, Double])((res, x) => res += x -> {if (res contains x) res(x) + 1 else 1})
    .map(t => (t._1, t._2 / input.size)).toMap

  def cumulative(distribution : Map[Char, Double]) =
    distribution.tail.foldLeft(distribution.head :: Nil)((res, x) =>(x._1 -> {x._2 + res.head._2}) :: res).toMap

  def intervals(distribution : Map[Char, Double]) = for (prob <- cumulative(distribution))
    yield {prob._1 -> (prob._2 - distribution(prob._1), prob._2)}

  def roundedLog(x : BigDecimal) : Int = {
    if (x < 0) throw new IllegalArgumentException("x < 0")
    def rough(y : BigInt, acc : Int) : Int = if (y <= 1) acc else rough(y >> 1, acc + 1)
    val y = rough(x toBigInt(), 0)
    y + {if (x > BigDecimal(BigInt(1) << y)) 1 else 0}
  }
}
