import collection.mutable
import scala.collection.mutable.HashMap
object Arithmetic {
	def log(curLetter : Char, interval : (Double, Double)) {
		println(curLetter + " " + interval._1 + " " + interval._2)
	}
	
	
	def encode(input:Iterable[Char], probabilities: mutable.HashMap[Char, (Double, Double)]) : String = {
		def encStep(iter : Iterator[Char], interval : (Double, Double)) : (Double, Double) = {
			if (!iter.hasNext) {			  
				return interval
			}
			val len = interval._2 - interval._1
			val letter = iter.next()
			val newInterval = (interval._1 + len * probabilities(letter)._1, interval._1 + len * probabilities(letter)._2)
			log(letter, newInterval)
			encStep(iter, newInterval)
		}
		val resInterval = encStep(input iterator, (0.0, 1.0)) 
		val x = (math.round((1 << 18) * (resInterval._2 + resInterval._1) / 2)).toBinaryString
		"0." + "0" * (18 - x.length()) + x
		
	}
	
	def main(args: Array[String]) {
	  val letterInterval = new mutable.HashMap[Char, (Double, Double)]
    letterInterval += (('a', (0.0, 0.2)), ('e', (0.2, 0.5)), ('i', (0.5, 0.6)),
	                     ('o', (0.6, 0.8)), ('u', (0.8, 0.9)), ('!', (0.9, 1.0)))
	  println(encode("eaii!", letterInterval))
	}
}