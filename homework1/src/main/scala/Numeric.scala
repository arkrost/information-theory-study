object Numeric {
  import EncodingStaff._

  /**
   * @return (F, G)
   */
  def encode(input : String) : Int = {
    def log(F : BigDecimal, G : BigDecimal, ch : Char) {
      Console println ("\\hline " + ch + " & " + F + " & " + G + " \\\\")
    }
    def step(F : BigDecimal, G : BigDecimal, input : String) : (BigDecimal, BigDecimal) =
      if (input.isEmpty) (F, G)
      else {
        log(F, G, input.head)
        val p = probabilities(input)
        val q = cumulative(p)
        step(F + q(input.head) * G, G * p(input.head), input.tail)
      }
    roundedLog(BigDecimal(1) / step(0, 1, input)._2)
  }

  def main(args : Array[String]) {
    val text = "Кукушка кукушонку купила капюшон. Как в капюшоне он смешон!"
    val tmp =  "2011021211"
    println(encode(text) + text.length)
  }
}
