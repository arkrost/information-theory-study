import java.io._
import scala.Console
import scala.math.log
import scala.collection.mutable
object Entropy {

  /**  Подсчитывает сколько раз встречается каждый "скользящий" блок в рассматриваемом вводе.
    *  @param  n       Длина "скользящего" блока
    *  @param  input   Входные данные
    */
  private def createStatistic[T](n : Int, input : Iterator[T]) : Map[Iterable[T], Double] = {
    val stats = mutable.Map.empty[Iterable[T], Double]                                         // Создаем пустую статистику
    input sliding(n) foreach(slide => {                                                        // Итерируемся по блокам длины n
      if (stats contains slide) stats(slide) = stats(slide) + 1 else  stats += slide -> 1      // Обновляем статистику
    })
    stats.toMap                                                                               // Возвращаем посчитанную статистику
  }

  def log2(x : Double) = log(x) / log(2)

  /**
   * @param f    Отображение
   * @param dist Вероятностное распределение
   * @return     Математическое ожидание M[f(dist)]
   */
  def M( f : Double => Double)(dist : TraversableOnce[Double]) = (dist map f).sum

  /**
   * @return  Энтропия
   */
  def H = M(p => -p * log2(p)) _

  /**
   * Подсчитывает энтропию, при условии что верояность невстреченных "скользящих" блоков нулевая.
   * @param n       Длина "скользящего" блока
   * @param input   Входные данные
   * @return        Энтропия
   */
  def H1[T](n: Int)(input : Iterator[T]) : Double = {
    val stats = createStatistic(n, input).values                   // Получаем частотную статистику
    val sum = stats.fold (0.0) ((x,y) => x + y)                    // Считаем количество блоков
    H(stats map (p => p/sum)) / n                                 // Нормируем распределение и считаем энтропию
  }

  /**
   * Подсчитывает энтропию, при условии что верояность невстреченных "скользящих" блоков пропорциональна 1/pow(N, n).
   * @param n       Длина "скользящего" блока
   * @param input   Входные данные
   * @return        Энтропия
   */
  def H2[T](n: Int)(input : Iterator[T]) : Double = {
    val stats = createStatistic(n, input).values                     // Получаем частотную статистику
    val sum = stats.fold (0.0) ((x,y) => x + y)                      // Считаем количество блоков
    val pAbsent = 1 / math.pow(sum + n - 1, n)                          //Вероятность невстретившегося блока
    val absent = math.pow(256, n) - stats.size                       // Считаем количество невстреченных блоков
    val denominator = absent * pAbsent + 1                                   // Находим нормирующий множитель
    (H(stats map (p => p /(sum * denominator))) + absent * H(List(pAbsent /denominator))) / n // Нормируем распределение и считаем энтропию встреченных блоков + добавка невстреченных блоков
  }

  def main(args : Array[String]) {
    val fileName = "PIC"
    def iterator = {
      val is = new DataInputStream(new BufferedInputStream(new FileInputStream(new File(fileName))))
      def tryRead : (Byte, Boolean) = try { (is.readByte, true) } catch {  case _ : IOException => {is.close;(0, false)} }
      Stream.continually(tryRead).takeWhile(_._2).map(_._1).iterator
    }
    (1 to 8) foreach(i => {
      Console println "H1" + i +" = " + H1(i)(iterator)
      Console println "H2" + i +" = " + H2(i)(iterator)
    })
  }
}
