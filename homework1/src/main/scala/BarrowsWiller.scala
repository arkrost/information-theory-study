package main.scala
import scala.Console
import scala.collection.mutable

object BarrowsWiller {

  def encode[T](n : Int, input : Iterator[T]): (String, Int) = {
    val perm = mutable.ListBuffer.empty[String]
    input sliding(n) foreach(slide => {
      perm += slide.mkString
    })
    val str = perm.head
    val sortedPerm = perm.toSeq.sortWith((x, y) => x < y)
    Console println  sortedPerm.zipWithIndex.foldRight("\n") ((x,y) => (x._2 + 1) +" & " + x._1.charAt(0) + " & " + x._1 + " & " + x._1.charAt(n - 1) + "\\\\ "+"\n" + "\\hline" + "\n" + y)
    (sortedPerm.foldLeft ("") ((x, y) => x + y.charAt(n - 1)), sortedPerm.indexWhere(x => x == str) + 1)
  }

  def main(args: Array[String]) {
    val input = "Кукушка_кукушонку_купила_капюшон._Как_в_капюшоне_он_смешон!"
    Console println(encode(input.size, input * 2 dropRight(1) iterator))
  }
}
